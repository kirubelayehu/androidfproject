package com.example.androidproject.Data;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class DataPageAdapter extends FragmentStatePagerAdapter {
    int numofitem;
    public DataPageAdapter(@NonNull FragmentManager fm,int position) {
        super(fm);
        this.numofitem=position;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

        switch (position){
            case 0:
                return new DataDailyFragment();
            case 1:
                return new DataWeeklyFragment();
            case 2:
                return new DataMonthlyFragment();
            case 3:
                return new DataNightFragment();
//            case 4:
//                return new DataWeekendFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numofitem;
    }
}
