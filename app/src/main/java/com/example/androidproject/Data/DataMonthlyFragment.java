package com.example.androidproject.Data;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.example.androidproject.R;


public class DataMonthlyFragment extends Fragment {
    String encodedHash = Uri.encode("#");

    public DataMonthlyFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view=inflater.inflate(R.layout.fragment_data_monthly_fragmenr, container, false);
        view.findViewById(R.id.monthlydata55birr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPackage(1);
            }
        });
        view.findViewById(R.id.monthlydata100birr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPackage(2);
            }
        });
        view.findViewById(R.id.monthlydata190birr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPackage(3);
            }
        });
        view.findViewById(R.id.monthlydata350birr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPackage(3);
            }
        });
        view.findViewById(R.id.monthlydata600birr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPackage(4);
            }
        });
        view.findViewById(R.id.monthlydata700birr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPackage(5);
            }
        });
        view.findViewById(R.id.monthlydata1300birr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPackage(6);
            }
        });
        view.findViewById(R.id.monthlydata1800birr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPackage(7);
            }
        });
        return  view;
    }


    private void getPackage(int num) {
        String callnum="999";
        String ussd = "*" + callnum +"*"+1+"*"+1+"*"+3+"*"+1+"*"+num+"*"+1 +encodedHash;
        startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ussd)));
        Log.e("NUm",String.valueOf(ussd));
        Log.e("Hash", encodedHash);
    }
}
