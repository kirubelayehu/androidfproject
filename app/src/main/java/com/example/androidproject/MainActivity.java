package com.example.androidproject;


import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

public class MainActivity extends AppCompatActivity {

    public  static final int RequestPermissionCode  = 2;
    String encodedHash = Uri.encode("#");
    public static final int REQUEST_CALL=1;
    AlertDialog callmedialog,rechargedialog,transferdialog;
    AlertDialog.Builder callmedialogBuilder,rechargedialogBuilder,transferdialogBuilder ;
    MaterialButton callmebtn,cancelcallmebtn,balance_recharge_btn,cancel_balance_recharge_btn,transferbtn,canceltransferbtn;
    ImageButton callmetocontactbtn,transfertocontactbtn;
    String contactnumber;
    TextInputEditText call_me_num ,balance_transfer_address;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    //Contact Permission Request
        EnableRuntimePermission();

        Log.e("Hash",encodedHash);
        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CALL_PHONE)!= PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(MainActivity.this,new String[]{Manifest.permission.CALL_PHONE},REQUEST_CALL);
        }
        //Call Me

        callmedialogBuilder= new AlertDialog.Builder(this);
        LayoutInflater callmeinflater = this.getLayoutInflater();
        final View call_me_dialogView = callmeinflater.inflate(R.layout.call_me_dialog, null);
        callmedialogBuilder.setView(call_me_dialogView);
        callmebtn=(MaterialButton)call_me_dialogView.findViewById(R.id.call_me_btn);
        cancelcallmebtn=(MaterialButton)call_me_dialogView.findViewById(R.id.cancel_btn);
        callmedialog=callmedialogBuilder.create();
        call_me_num=call_me_dialogView.findViewById(R.id.call_me_number);


        // balance recharge
        rechargedialogBuilder= new AlertDialog.Builder(this);
        LayoutInflater rechargeinflater = this.getLayoutInflater();
        final View rechage_dialogView = rechargeinflater.inflate(R.layout.balance_recharge_layout, null);
        rechargedialogBuilder.setView(rechage_dialogView);
        balance_recharge_btn=(MaterialButton)rechage_dialogView.findViewById(R.id.charge_btn);
        cancel_balance_recharge_btn=(MaterialButton)rechage_dialogView.findViewById(R.id.balance_recharge_cancel_btn);
        rechargedialog=rechargedialogBuilder.create();
        final TextInputEditText recharge_number=rechage_dialogView.findViewById(R.id.charge_number_field);


        //
        //Read contacts
        callmetocontactbtn=(ImageButton)call_me_dialogView.findViewById(R.id.callmetocontact);
        callmetocontactbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent contactintent=new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(contactintent,7);

            }
        });
        //transfer
        transferdialogBuilder= new AlertDialog.Builder(this);
        LayoutInflater transferinflater = this.getLayoutInflater();
        final View transfer_dialogView = transferinflater.inflate(R.layout.balance_tansfer_layout, null);
        transferdialogBuilder.setView(transfer_dialogView);
        transferbtn=(MaterialButton)transfer_dialogView.findViewById(R.id.balance_transfer_btn);
        canceltransferbtn=(MaterialButton)transfer_dialogView.findViewById(R.id.balance_transfer_cancel_btn);
        transferdialog=transferdialogBuilder.create();
        balance_transfer_address=transfer_dialogView.findViewById(R.id.to_who);
        final TextInputEditText balance_transfer_amount=transfer_dialogView.findViewById(R.id.amout);


        //ReadContacttotransfer
        transfertocontactbtn=(ImageButton)transfer_dialogView.findViewById(R.id.transfertocontact);
        transfertocontactbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent contactintent=new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(contactintent,7);

            }
        });

        //Listener areas
        findViewById(R.id.check_balance).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { checkBalance();
            }
        });
        findViewById(R.id.call_me).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callmedialog.show();
                final String callmenum=call_me_num.getText().toString();
                if (callmenum.isEmpty() && callmenum.length()<1)
                callmebtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       callme(callmenum);
                       callmedialog.dismiss();
                    }
                });
                cancelcallmebtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callmedialog.dismiss();
                    }
                });
            }
        });


        //Balance recharge Listener Area
        findViewById(R.id.balance_recharge).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String  code=recharge_number.getText().toString();

                rechargedialog.show();
                if (code.length()<14 && !code.isEmpty() && TextUtils.isDigitsOnly(code)){
                    balance_recharge_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        balance_Recharge(code);
                        rechargedialog.dismiss();
                    }});
                }
                else {balance_recharge_btn.setVisibility(View.INVISIBLE);
                    Toast.makeText(MainActivity.this, "Invalid Format !", Toast.LENGTH_SHORT).show();
                }
                cancel_balance_recharge_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        rechargedialog.dismiss();
                    }});
            }
        });

        //transfer Listener
        findViewById(R.id.transfer_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String amount=balance_transfer_amount.getText().toString();
                final String address=balance_transfer_address.getText().toString();
                transferdialog.show();
                if (amount.length()<1 && amount .isEmpty() && !TextUtils.isDigitsOnly(amount) && address.length()<10 && address.isEmpty()){
                transferbtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        balance_transfer(Integer.parseInt(amount),address);
                        transferdialog.dismiss();
                    }
                });}
                else{
                    Toast.makeText(MainActivity.this, "Invalid Format", Toast.LENGTH_SHORT).show();
                }
                canceltransferbtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        transferdialog.dismiss();
                    }
                });
            }
        });
        findViewById(R.id.packageoffer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,PackageActivity.class));
            }
        });
        findViewById(R.id.loan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,LoanActvity.class));
            }
        });
    }


    public void EnableRuntimePermission(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                Manifest.permission.READ_CONTACTS))
        {

            Toast.makeText(MainActivity.this,"CONTACTS permission allows us to Access CONTACTS app", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(MainActivity.this,new String[]{
                    Manifest.permission.READ_CONTACTS}, RequestPermissionCode);

        }
    }

    private void balance_transfer(int amount, String address) {
        String callnum="806";

        String ussd = "*" + callnum +"*"+address+"*"+amount +encodedHash ;
        Toast.makeText(this, ussd, Toast.LENGTH_SHORT).show();
        Log.e("USSD",ussd);
        startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ussd)));
    }

    private void balance_Recharge(String code) {
            String callnum="807";
            String ussd = "*" + callnum +"*"+code +encodedHash;
            startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ussd)));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode==REQUEST_CALL){
            if (grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED ){
            }
            else{
                Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
            }
        }
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            Toast.makeText(MainActivity.this,"Permission Granted, Now your application can access CONTACTS.", Toast.LENGTH_LONG).show();

        } else {

            Toast.makeText(MainActivity.this,"Permission Canceled, Now your application cannot access CONTACTS.", Toast.LENGTH_LONG).show();

        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {

            Uri uri;
            Cursor cursor1, cursor2;
            String TempNameHolder, TempNumberHolder, TempContactID, IDresult = "" ;
            int IDresultHolder ;

            uri = data.getData();

            cursor1 = getContentResolver().query(uri, null, null, null, null);

            if (cursor1.moveToFirst()) {

                TempNameHolder = cursor1.getString(cursor1.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                TempContactID = cursor1.getString(cursor1.getColumnIndex(ContactsContract.Contacts._ID));

                IDresult = cursor1.getString(cursor1.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                IDresultHolder = Integer.valueOf(IDresult) ;

                if (IDresultHolder == 1) {

                    cursor2 = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + TempContactID, null, null);

                    while (cursor2.moveToNext()) {

                        TempNumberHolder = cursor2.getString(cursor2.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                        Log.e("Contact", TempContactID+"  "+TempNameHolder+"  "+TempNumberHolder );
                        contactnumber=TempNumberHolder.toString();
                    }

                }
                if (callmedialog.isShowing()){
                    call_me_num.setText(contactnumber);
                    Log.e("Call Me Num",call_me_num.getText().toString());
                }
                else if (transferdialog.isShowing()){
                    balance_transfer_address.setText(contactnumber);
                    Log.e("Transfer  Num",balance_transfer_address.getText().toString());

                }


            }
        }


}


    private void checkBalance() {
       String callnum="804";
        String ussd = "*" + callnum + encodedHash;
        startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ussd)));
    }
    private void callme(String num) {
        String callnum="807";
        String ussd = "*" + callnum +"*"+num +encodedHash;
        startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ussd)));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main_option_menu,menu);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int index=item.getItemId();
        switch (index){
            case R.id.helpme:
                startActivity(new Intent(MainActivity.this,HelpActivity.class));
                break;
            case R.id.aboutus:
                startActivity(new Intent(MainActivity.this,AboutActivity.class));
                break;
            case R.id.setting:
                startActivity(new Intent(MainActivity.this,SettingActivity.class));
                break;
        }
        return false;
    }
}
