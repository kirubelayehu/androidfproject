package com.example.androidproject;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class PackagePageAdapter extends FragmentPagerAdapter {

    private int numofitem;

    public PackagePageAdapter(FragmentManager fm, int numofitem) {
        super(fm);
        this.numofitem=numofitem;

    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new VoiceFragment();
            case 1:
                return new DataFragment();
            case 2:
                return new SmsFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numofitem;
    }
}
