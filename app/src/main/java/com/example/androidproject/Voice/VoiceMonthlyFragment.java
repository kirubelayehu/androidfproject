package com.example.androidproject.Voice;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.example.androidproject.R;

public class VoiceMonthlyFragment extends Fragment {
    String encodedHash = Uri.encode("#");

    public VoiceMonthlyFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_voice_monthly, container, false);

        view.findViewById(R.id.monthly40birr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPackage(1);
            }
        });
        view.findViewById(R.id.monthly60birr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPackage(2);
            }
        });
        view.findViewById(R.id.monthly100birr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPackage(3);
            }
        });
        view.findViewById(R.id.monthly140birr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPackage(4);
            }
        });
        view.findViewById(R.id.monthly150birr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPackage(5);
            }
        });
        view.findViewById(R.id.monthly200birr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPackage(6);
            }
        });
        view.findViewById(R.id.monthly250birr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPackage(7);
            }
        });
        view.findViewById(R.id.monthly270birr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPackage(8);
            }
        });
        view.findViewById(R.id.monthly300birr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPackage(9);
            }
        });
        view.findViewById(R.id.monthly350birr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPackage(10);
            }
        });
        view.findViewById(R.id.monthly400birr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPackage(11);
            }
        });
        view.findViewById(R.id.monthly450birr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPackage(12);
            }
        });
        view.findViewById(R.id.monthly500birr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPackage(13);
            }
        });
        view.findViewById(R.id.monthly540birr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPackage(14);
            }
        });
        view.findViewById(R.id.monthly600birr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPackage(15);
            }
        });
        view.findViewById(R.id.monthly1350birr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPackage(16);
            }
        });
        return view;

    }

    private void getPackage(int num) {
        String callnum="999";
        String ussd = "*" + callnum +"*"+1+"*"+1+"*"+1+"*"+4+"*"+num+"*"+1+encodedHash;
        startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ussd)));
        Log.e("NUm",String.valueOf(ussd));
        Log.e("Hash", encodedHash);
    }
    private void forwardPackage(int num,int mobnum){

    }
}
