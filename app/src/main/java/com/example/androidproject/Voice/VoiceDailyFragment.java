package com.example.androidproject.Voice;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.example.androidproject.R;


public class VoiceDailyFragment extends Fragment {

    String encodedHash = Uri.encode("#");


    public VoiceDailyFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view  = inflater.inflate(R.layout.fragment_voice_daily, container, false);

        view.findViewById(R.id.daily3birr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getpackage(1);
            }
        });
        view.findViewById(R.id.daily5birr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getpackage(2);
            }
        });
        view.findViewById(R.id.daily10birr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getpackage(3);
            }
        });

        return view;
    }

    private  void getpackage(int num){

        String callnum="999";
        String ussd = "*" + callnum +"*"+1+"*"+1+"*"+1+"*"+2+"*"+num+"*"+1 +encodedHash;
        startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ussd)));
        Log.e("NUm",String.valueOf(ussd));
        Log.e("Hash", encodedHash);
    }
    private  void forwardpackage(int num,int mobnum){

    }

}
