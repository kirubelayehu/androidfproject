package com.example.androidproject.Voice;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.example.androidproject.R;


public class VoiceNightFragment extends Fragment {
    String encodedHash = Uri.encode("#");

    public VoiceNightFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_voice_night, container, false);

        view.findViewById(R.id.night3birr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPackage(1);
            }
        });
        view.findViewById(R.id.night4birr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPackage(2);
            }
        });
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPackage(3);
            }
        });
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPackage(4);
            }
        });


        return  view;


    }

    private void getPackage(int num) {
        String callnum="999";
        String ussd = "*" + callnum +"*"+1+"*"+1+"*"+1+"*"+1+"*"+num+"*"+1 +encodedHash;
        startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ussd)));
        Log.e("NUm",String.valueOf(ussd));
        Log.e("Hash", encodedHash);
    }
}
