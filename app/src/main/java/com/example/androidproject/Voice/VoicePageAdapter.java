package com.example.androidproject.Voice;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class VoicePageAdapter extends FragmentPagerAdapter {

    private int numofitem;

    public VoicePageAdapter(@NonNull FragmentManager fm, int numofitem) {
        super(fm);
        this.numofitem=numofitem;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new VoiceDailyFragment();
            case 1:
                return new VoiceWeeklyFragment();
            case 2:
                return new VoiceMonthlyFragment();
            case 3:
                return new VoiceNightFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numofitem;
    }
}
