package com.example.androidproject;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.example.androidproject.Voice.VoicePageAdapter;
import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;


public class VoiceFragment extends Fragment {
    TabLayout voicetabLayout;
    ViewPager voicepager;
    VoicePageAdapter voicepackagePageAdapter;
    TabItem daily,weekly,monthly,night;
    public VoiceFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_voice, container, false);
        voicetabLayout=view.findViewById(R.id.tablayoutvoice);

        daily =view.findViewById(R.id.itemvoicedaily);
        weekly=view.findViewById(R.id.itemvoiceweekly);
        monthly=view.findViewById(R.id.itemvoicemonthly);
        night=view.findViewById(R.id.itemvoicenight);
        voicepager=view.findViewById(R.id.voicepager);

        voicepackagePageAdapter=new VoicePageAdapter(getFragmentManager(),voicetabLayout.getTabCount());
        voicepager.setAdapter(voicepackagePageAdapter);
        voicepager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(voicetabLayout));
        voicetabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                voicepager.setCurrentItem(tab.getPosition());
                Log.e("Pos",String.valueOf(tab.getPosition()));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        return view;

    }
}
