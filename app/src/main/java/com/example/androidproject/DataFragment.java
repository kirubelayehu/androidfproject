package com.example.androidproject;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.example.androidproject.Data.DataPageAdapter;
import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;


public class DataFragment extends Fragment {

    TabLayout datatabLayout;
    ViewPager datapager;
    DataPageAdapter datapackagePageAdapter;
    TabItem daily,weekly,monthly,night;
    public DataFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_data, container, false);
        datatabLayout=view.findViewById(R.id.tablayoutdata);

        daily =view.findViewById(R.id.itemdatadaily);
        weekly=view.findViewById(R.id.itemdataweekly);
        monthly=view.findViewById(R.id.itemdatamonthly);
        night=view.findViewById(R.id.itemdatanight);
        datapager=view.findViewById(R.id.datapager);

        datapackagePageAdapter=new DataPageAdapter(getFragmentManager(),datatabLayout.getTabCount());
        datapager.setAdapter(datapackagePageAdapter);
        datapager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(datatabLayout));
        datatabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                datapager.setCurrentItem(tab.getPosition());
                Log.e("Pos",String.valueOf(tab.getPosition()));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        return view;

    }

}
