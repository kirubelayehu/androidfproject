package com.example.androidproject;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.example.androidproject.Sms.SmsPageAdapter;
import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;

public class SmsFragment extends Fragment {

    TabLayout smstabLayout;
    ViewPager smspager;
    SmsPageAdapter smspackagePageAdapter;
    TabItem daily,weekly,monthly,night;
    public SmsFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_sms, container, false);
        smstabLayout=view.findViewById(R.id.tablayoutsms);

        daily =view.findViewById(R.id.itemsmsdaily);
        weekly=view.findViewById(R.id.itemsmsweekly);
        monthly=view.findViewById(R.id.itemsmsmonthly);
        smspager=view.findViewById(R.id.smspager);

        smspackagePageAdapter=new SmsPageAdapter(getFragmentManager(),smstabLayout.getTabCount());
        smspager.setAdapter(smspackagePageAdapter);
        smspager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(smstabLayout));
        smstabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                smspager.setCurrentItem(tab.getPosition());
                Log.e("Pos",String.valueOf(tab.getPosition()));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        return view;

    }

}
