package com.example.androidproject;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;

public class PackageActivity extends AppCompatActivity {

    Toolbar toolbar;
    TabLayout tabLayout;
    ViewPager pager;
    PackagePageAdapter packagePageAdapter;
    TabItem voicetab,datatab,smstab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_package);

//        toolbar=findViewById(R.id.toolbar);
        tabLayout=findViewById(R.id.tablayout);
//        toolbar.setTitle("Package");
//        setSupportActionBar(toolbar);
//
        voicetab =findViewById(R.id.itemvoice);
        datatab=findViewById(R.id.itemdata);
        smstab=findViewById(R.id.itemsms);
        pager=(ViewPager)findViewById(R.id.pager);

        packagePageAdapter=new PackagePageAdapter(getSupportFragmentManager(),tabLayout.getTabCount());
        pager.setAdapter(packagePageAdapter);
        pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
            pager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }
}
