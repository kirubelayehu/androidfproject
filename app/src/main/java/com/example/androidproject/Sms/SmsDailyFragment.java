package com.example.androidproject.Sms;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.example.androidproject.R;

public class SmsDailyFragment extends Fragment {

    String encodedHash = Uri.encode("#");


    public SmsDailyFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_sms_daily, container, false);

        view.findViewById(R.id.smsdail2birr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSmsPackage(1);
            }
        });
        view.findViewById(R.id.smsdaily3birr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSmsPackage(2);
            }
        });
        view.findViewById(R.id.smsdail5birr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSmsPackage(3);
            }
        });
        return view;

    }

    private void getSmsPackage(int num) {

        String callnum="999";
        String ussd = "*" + callnum +"*"+1+"*"+1+"*"+3+"*"+1+"*"+num+"*"+1 +encodedHash;
        startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ussd)));
        Log.e("NUm",String.valueOf(ussd));
        Log.e("Hash", encodedHash);
    }
}
