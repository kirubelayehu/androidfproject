package com.example.androidproject.Sms;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class SmsPageAdapter extends FragmentStatePagerAdapter {

    int numofitem;

    public SmsPageAdapter(@NonNull FragmentManager fm , int position) {
        super(fm);
        this.numofitem=position;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new SmsDailyFragment();
            case 1:
                return new SmsFragmenrWeekly();
            case 2:
                return new SmsMonthlyFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numofitem;
    }
}
