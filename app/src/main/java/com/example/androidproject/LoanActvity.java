package com.example.androidproject;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

public class LoanActvity extends AppCompatActivity {
    Uri uri ;
    String encodedHash = Uri.encode("#");
    Spinner loanamountspinner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loan_actvity);
        loanamountspinner=findViewById(R.id.loanamount);
        final String amount=loanamountspinner.getSelectedItem().toString();
        findViewById(R.id.loanbtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              int loanamout=Integer.parseInt(amount);
                makeloan(loanamout);
            }
        });

    }

    private void makeloan(int loanamout) {
        String callnum="810";
        String ussd = "*" +callnum +1+loanamout +"*"+1+ encodedHash;
        startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ussd)));
    }
}
